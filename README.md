# dockerized LEMP stack

A LEMP stack realized with docker containers.

## Installation

#### 0. Prerequisities
Install `docker`, `docker-compose` and `git` on your system. Then clone this repo to some `<convenient path>`.

#### 1. Build the php image
Build the php-fpm image with mysqli installed
```shell
cd <path/to/repo>/build_phpfpm
docker build -t phpfmp_mysqli:latest .
```

#### 2. Create and start the containers
Simply use `docker-compose`:
```shell
cd <path/to/repo>
docker-compose up -d
```
By default, port 80 is exposed for nginx and `www/` is used as the base for the website.

#### 3. Finishing
Change the password for the root user of the database and create a user for website queries. Enter the database by
```shell
docker exec -it lemp_mariadb mysql -u root -pALTER USER 'root'@'localhost' IDENTIFIED BY 's3cr3t-p4ssw0rd';
```
Then run
~~~~sql
ALTER USER 'root'@'localhost' IDENTIFIED BY 's3cr3t-p4ssw0rd';
ALTER USER 'root'@'%' IDENTIFIED BY 's3cr3t-p4ssw0rd';
CREATE USER 'website'@'%' IDENTIFIED BY '0th3r-s3cr3t-p4ssw0rd';
~~~~
After having creted the database for the website, make sure to grant permissions in it 
to the website user.

#### 4. Testing
In your webbrowser navigate to `http://<ip_of_host>` and you should be greated with the demo page.

## Updating
For the php container, the image needs to be regenerated, so:
```shell
cd <path/to/repo>/build_phpfpm
docker build -t phpfpm_mysqli:latest .
```

After this to update the containers, simply remove and then recreate them again
```shell
cd <path/to/repo>
docker-compose down
docker-compose up -d
```


